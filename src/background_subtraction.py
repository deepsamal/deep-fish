from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
# from __future__ import unicode_literals
import os
import sys
import cv2
#from PIL import Image
import numpy as np
import time
#from mr_saliency import MR
#import pyflow
import matplotlib.pyplot as plt
import random
import imageio

images_directory = '../data/'
input_file = '/home/cpslab/deep-fish/data/scoop.mp4'
output_directory = '../output/'

save_output = True

fgbg = cv2.createBackgroundSubtractorMOG2()

# it will extract the image list 
def get_img_lists(img_path):
    frame_list = []
    for frame in os.listdir(img_path):
        if os.path.splitext(frame)[1] == '.png':
            frame_list.append(os.path.join(img_path, frame)) 
    return frame_list

def subtract_background():
    frame_lists = get_img_lists(images_directory)
    frame_lists.sort()

    for idx in range(len(frame_lists)):
        current_frame = cv2.imread(frame_lists[idx])
        fg_mask = fgbg.apply(current_frame)

        # visualize foreground...
        #fg_mask[fg_mask>120] = 255
        cv2.imshow('foreground', fg_mask)
        cv2.waitKey(100)

        if save_output:
            frame_path = output_directory
            if not os.path.exists(frame_path):
                os.mkdir(frame_path)
            cv2.imwrite(frame_path + str(idx).zfill(5) + '.png', fg_mask)

subtract_background()

#ffmpeg -framerate 10 -i %05d.png foreground.mp4
